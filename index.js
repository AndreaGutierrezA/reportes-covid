"use strict";
exports.__esModule = true;
var fs = require("fs");
var path = require("path");
var csv = require("fast-csv");
require("dotenv/config");
var clc = require("cli-color");
var data;
var states = new Object();
fs.createReadStream(path.resolve(__dirname, process.env.FILE_NAME))
    .pipe(csv.parse({ headers: true }))
    .on("error", function (error) { return console.error(error); })
    .on("data", function (row) {
    var _a, _b;
    var provinceState = row["Province_State"];
    var accumPopulation = parseInt(row["Population"]);
    var accumDeaths = parseInt(row[process.env.DATE_TO_REQUEST]);
    states[provinceState] = {
        accumPopulation: (((_a = states[provinceState]) === null || _a === void 0 ? void 0 : _a.accumPopulation) ? states[provinceState].accumPopulation
            : 0) + accumPopulation,
        accumDeaths: (((_b = states[provinceState]) === null || _b === void 0 ? void 0 : _b.accumDeaths) ? states[provinceState].accumDeaths
            : 0) + accumDeaths
    };
})
    .on("end", function (rowCount) {
    console.log(clc.magenta.underline('Resumen general'));
    Object.keys(states).forEach(function (state) {
        if (states[state].accumPopulation !== 0) {
            states[state].percentage = ((states[state].accumDeaths / states[state].accumPopulation) * 100).toFixed(3) + '%';
        }
        else {
            delete states[state];
        }
    });
    console.table(states);
    console.log(clc.red.underline('Estado con mayor acumulado : ', clc.yellow.underline(Object.keys(states).reduce(function (a, b) { return states[a].accumDeaths > states[b].accumDeaths ? a : b; }))));
    console.log(clc.green.underline('Estado con menor acumulado : ', clc.cyan.underline(Object.keys(states).reduce(function (a, b) { return states[a].accumDeaths < states[b].accumDeaths ? a : b; }))));
    console.log(rowCount + " registros fueron procesados");
});
