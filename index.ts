import * as fs from "fs";
import * as path from "path";
import * as csv from "fast-csv";
import "dotenv/config";
import * as clc from "cli-color";

let data: { provinceState: string; population: string; accum: number };
let states = new Object();
fs.createReadStream(path.resolve(__dirname, process.env.FILE_NAME))
    .pipe(csv.parse({ headers: true }))
    .on("error", (error) => console.error(error))
    .on("data", (row) => {
        const provinceState = row["Province_State"];
        const accumPopulation = parseInt(row["Population"]);
        const accumDeaths = parseInt(row[process.env.DATE_TO_REQUEST]);

        states[provinceState] = {
            accumPopulation:
                (states[provinceState]?.accumPopulation
                    ? states[provinceState].accumPopulation
                    : 0) + accumPopulation,
            accumDeaths:
                (states[provinceState]?.accumDeaths
                    ? states[provinceState].accumDeaths
                    : 0) + accumDeaths,
        };
    })
    .on("end", (rowCount: number) => {
        console.log(clc.magenta.underline('Resumen general'));
        Object.keys(states).forEach((state) => {
            if (states[state].accumPopulation !== 0) {
                states[state].percentage = ((states[state].accumDeaths / states[state].accumPopulation) * 100).toFixed(3) +'%';
            }else{
                delete states[state];
            }

        });
        console.table(states);
        console.log(clc.red.underline('Estado con mayor acumulado : ', clc.yellow.underline(Object.keys(states).reduce((a, b) => states[a].accumDeaths > states[b].accumDeaths ? a : b))))
        console.log(clc.green.underline('Estado con menor acumulado : ',clc.cyan.underline(Object.keys(states).reduce((a, b) => states[a].accumDeaths < states[b].accumDeaths ? a : b))))
        console.log(`${rowCount} registros fueron procesados`);
    });
